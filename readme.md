# matetest #

## installation ##
Enter the following commands:

```bash
git clone https://codeberg.org/eichkat3r/minetest-mate
cp -r minetest-mate ~/.minetest/mods/
```

After that, run Minetest. Create a new world and select the options menu
before entering the world for the first time.
Activate the mate mod. Now you can enjoy mate plants, mate leaves and
mate bottles.
