minetest.register_craftitem("mate:mateleaves", {
	description = "leaves of a mate plant",
	inventory_image = "mateleaves.png",
})

minetest.register_craftitem("mate:matebottle", {
	description = "bottle of mate",
	inventory_image = "matebottle.png",
	on_use = minetest.item_eat(10, "vessels:glass_bottle")
})

minetest.register_craft({
	type = "shapeless",
	output = "mate:matebottle",
	recipe = {
		"vessels:glass_bottle",
		"mate:mateleaves",
		"bucket:bucket_water"
	},
})

minetest.register_node("mate:mateplant", {
	drawtype = "plantlike",
	waving = 1,
	sunlight_propagates = true,
	paramtype = "light",
	walkable = false,
	buildable_to = true,
	groups = { snappy = 3, flammable = 3, attached_node = 1 },
	tiles = { "mateplant.png" },
	selection_box = {
		type = "fixed",
		fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 },
	},
	sounds = default.node_sound_leaves_defaults(),
	drop = "mate:mateleaves",
})

minetest.register_decoration({
	deco_type = "simple",
	place_on = {
		"default:dirt",
		"default:sand",
		"default:desert_sand",
		"default:dirt_with_grass"
	},
	sidelen = 16,
	fill_ratio = 0.001,
	decoration = "mate:mateplant",
	biomes = {"deciduous_forest_ocean", "sandstone_grassland_ocean",
			"rainforest_swamp", "savanna_ocean", "desert_ocean"},
	height = 1,
})

